import yt_dlp as youtube_dl
import subprocess
import time
import os
import sys


video_url = sys.argv[1]


def download_and_extract_audio(url):
    ydl_opts = {
        'format': 'bestaudio/best',
        'outtmpl': '/app/results/%(title)s.%(ext)s',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        info_dict = ydl.extract_info(url, download=False)
        video_title = info_dict.get('title', None)
        chapters = info_dict.get('chapters', [])
        is_playlist = 'entries' in info_dict

        if is_playlist:
            playlist_title = info_dict['title']
            playlist_entries = info_dict['entries']

            print(f"The video is part of the playlist: {playlist_title}")

            for entry in playlist_entries:
                video_title = entry['title']
                playlist_entry_url = entry['webpage_url']
                info_dict = ydl.extract_info(
                    playlist_entry_url, download=False)
                video_title = info_dict.get('title', None)
                chapters = info_dict.get('chapters', [])

                downloadVideoAndSplitInChapters(
                    playlist_entry_url, ydl, video_title, chapters)
        else:
            print("The video is not part of a playlist.")
            downloadVideoAndSplitInChapters(url, ydl, video_title, chapters)


def downloadVideoAndSplitInChapters(url, ydl, video_title, chapters):
    if video_title:
        if chapters:
            print("The video has chapters.")
            ydl_opts_no_mp3 = {
                'format': 'bestaudio/best',
                'outtmpl': '/app/tempfile',
                'postprocessors': [{
                    'key': 'FFmpegExtractAudio',
                    'preferredcodec': 'mp3',
                    'preferredquality': '192',
                }]
            }

            with youtube_dl.YoutubeDL(ydl_opts_no_mp3) as ydl:
                ydl.download([url])
            
            for chapter in chapters:                  
                chaptertitle = chapter.get('title')
                start = chapter.get('start_time')
                end = chapter.get('end_time')
                ffmpeg_command = [
                    'ffmpeg',
                    '-i', '/app/tempfile.mp3',
                    '-ss', str(start),
                    '-to', str(end),
                    '-c', 'copy',
                    '/app/results/{}-{}.mp3'.format(video_title, chaptertitle)
                ]

                subprocess.call(ffmpeg_command)  
                #subprocess.run(["rm","-f", "/app/results/tempfile.mp3"])
                print(f"Section extracted: {'/app/results/%(title)s-{}s.%(ext)s'.format(chaptertitle)}")
         #       downloadChapter(url, chapter, ydl)
        else:
            print("The video does not have chapters.")
            ydl.download([url])
            print("Extraction completed successfully.")
    else:
        print("Unable to retrieve video title.")


def downloadChapter(url, chapter, ydl):
    chaptertitle = chapter.get('title')

    # Set the options for downloading the specific section
    start = chapter.get('start_time')
    end = chapter.get('end_time')
    ydl_opts_chapter = {
        'format': 'bestaudio/best',
        'outtmpl': '/app/results/%(title)s-{}s.%(ext)s'.format(chaptertitle),
        'start_time': start,
        'end_time': end,
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }]
    }
    print("Downloading video section {} from {} to {} seconds.".format(
        chaptertitle, start, end))

    # Download the section using the specific options
    with youtube_dl.YoutubeDL(ydl_opts_chapter) as ydl:
        ydl.download([url])

    print(f"Section downloaded: '{chaptertitle}'")


download_and_extract_audio(video_url)
