# Build

``` shell
##############################
########## BUILD #############
##############################

# build
docker build -t  yt2mp3:v1 .

# run with volume
docker run --rm -v c/music/download:/app/results --name yt2mp3 yt2mp3:v1 "https://www.youtube.com/watch?v=PrbpM6CAXYU"

##############################
########## MAINTAIN ##########
##############################

# remove leftover containers
docker container prune

# remove only temp images
docker image prune

# remove all images
docker image prune --all

##############################
########## OTHER #############
##############################

# update locally
pip install -r requirements.txt --upgrade

```