FROM python:3.11-slim-bookworm

# Install ffmpeg
RUN apt-get update && apt-get install -y ffmpeg

WORKDIR /app/

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY ./app.py .

ENTRYPOINT [ "python", "./app.py" ]